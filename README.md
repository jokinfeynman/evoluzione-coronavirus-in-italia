# Evoluzione del COVID-19 in Italia

Elaborazione dei dati forniti [dalla protezione civile](https://github.com/pcm-dpc/COVID-19) riguardanti l'epidemia di coronavirus italiana.

## Evoluzione spaziale

Notare la colorbar, che è troncata e logaritmica.

**Densità casi**

La gif sottostante mostra l'evoluzione della densità dei casi, definita come il rapporto tra i casi totali e la popolazione, per ogni provincia

![Evoluzione densita](img/densita_casi.gif)

**Densità nuovi casi**

Come la precedente, ma mostra i nuovi casi per ogni giorno.

![Evoluzione densita](img/densita_nuovi.gif)

**Nuovi casi**

![Evoluzione spaziale](img/evoluzione_spaziale.gif)

**Casi totali**

![Evoluzione spaziale](img/casi_totali.gif)

## Acknowledgments

* Il file `province.geojson` proviene da [openpolis/geojson-italy](https://github.com/openpolis/geojson-italy) 
* I dati sulla popolazione delle province provengono dall'[ISTAT](http://dati.istat.it/Index.aspx?DataSetCode=DCIS_POPRES1) (sono dati del 2011)

